<?php

namespace common\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "wiki_user".
 *
 * @property int $id
 * @property int $id_wiki
 * @property int $id_user
 * @property int $created_at
 * @property int $updated_at
 */
class WikiUser extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'wiki_user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_wiki', 'id_user', 'created_at', 'updated_at'], 'required'],
            [['id_wiki', 'id_user', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_wiki' => Yii::t('app', 'Id Wiki'),
            'id_user' => Yii::t('app', 'Id User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

}
