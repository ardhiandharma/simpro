<?php

namespace common\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property string $description
 * @property int $start_plan_at
 * @property int $end_plan_at
 * @property int $start_actual_at
 * @property int $end_actual_at
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 * @property Task $tasks
 * @property Meeting $meetings
 * @property User $user
 * @property int[] $memberIds
 */
class Project extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'name', 'start_plan_at', 'end_plan_at', 'status', 'created_at', 'updated_at'], 'required'],
            [['id_user', 'start_actual_at', 'end_actual_at', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 10],
            [['end_plan_at'], 'validateDate']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'name' => Yii::t('app', 'Project'),
            'description' => Yii::t('app', 'Description'),
            'start_plan_at' => Yii::t('app', 'Start Date'),
            'end_plan_at' => Yii::t('app', 'Deadline'),
            'start_actual_at' => Yii::t('app', 'Start Actual At'),
            'end_actual_at' => Yii::t('app', 'End Actual At'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->start_plan_at = strtotime($this->start_plan_at);
            $this->end_plan_at = strtotime($this->end_plan_at);
            return true;
        } else {
            return false;
        }
    }

    public function getTasks() {
        return $this->hasMany(Task::className(), ['id_project' => 'id']);
    }

    public function getOnGoingTasks() {
        return $this->hasMany(Task::className(), ['id_project' => 'id'])->where(['is not', 'start_actual_at', NULL]);
    }

    public function getCompletedTasks() {
        return $this->hasMany(Task::className(), ['id_project' => 'id'])->where(['is not', 'end_actual_at', NULL]);
    }

    public function getMeetings() {
        return $this->hasMany(Meeting::className(), ['id_project' => 'id']);
    }

    public function getMembers() {
        return $this->hasMany(User::className(), ['id' => 'id_user'])
                        ->viaTable('project_user', ['id_user' => 'id']);
    }

    public function getMemberIds() {
        $ids = [];
        $ids[] = $this->id_user;
        foreach (ProjectUser::find()->where(['id_project' => $this->id])->all()as $value) {
            $ids[] = $value->id_user;
        }

        return $ids;
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function validateDate($attribute, $params, $validator) {
        if (strtotime($this->end_plan_at) < strtotime($this->start_plan_at)) {
            $this->addError($attribute, Yii::t('app', 'Deadline smaller than start date'));
        }
    }

}
