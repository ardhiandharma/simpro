<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task_user".
 *
 * @property int $id
 * @property int $id_task
 * @property int $id_user
 * @property int $created_at
 * @property int $updated_at
 */
class TaskUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_task', 'id_user', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_task' => Yii::t('app', 'Id Task'),
            'id_user' => Yii::t('app', 'Id User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
