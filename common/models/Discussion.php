<?php

namespace common\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "discussion".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_project
 * @property string $subject
 * @property string $message
 * @property int $created_at
 * @property int $updated_at
 */
class Discussion extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'discussion';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'id_project', 'subject', 'created_at', 'updated_at'], 'required'],
            [['id_user', 'id_project', 'created_at', 'updated_at'], 'integer'],
            [['message'], 'string'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_project' => Yii::t('app', 'Id Project'),
            'subject' => Yii::t('app', 'Subject'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }

    public function getComments() {
        return $this->hasMany(Comment::className(), ['entityId' => 'id']);
    }

}
