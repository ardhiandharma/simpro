<?php

namespace common\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "meeting".
 *
 * @property int $id
 * @property int $id_project
 * @property int $id_notulen
 * @property string $name
 * @property string $agenda
 * @property int $start_at
 * @property int $end_at
 * @property string $summary
 * @property int $created_at
 * @property int $updated_at
 * @property Project project
 * @property string $location 
 * @property int $start_actual_at
 * @property int $end_actual_at
 * @property string $status 
 * @property string $file
 */
class Meeting extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'meeting';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_project', 'name', 'agenda', 'start_at', 'end_at', 'created_at', 'updated_at', 'location', 'status'], 'required'],
            [['id_project', 'id_notulen', 'created_at', 'start_actual_at', 'end_actual_at'], 'integer'],
            [['agenda', 'summary'], 'string'],
            [['name', 'file'], 'string', 'max' => 100],
            [['location'], 'string', 'max' => 255],
            [['end_at'], 'validateDate']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_project' => Yii::t('app', 'Id Project'),
            'id_notulen' => Yii::t('app', 'Notulen'),
            'name' => Yii::t('app', 'Name'),
            'agenda' => Yii::t('app', 'Agenda'),
            'start_at' => Yii::t('app', 'Start At'),
            'end_at' => Yii::t('app', 'End At'),
            'summary' => Yii::t('app', 'Summary'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Created At'),
            'location' => Yii::t('app', 'Location'),
            'start_actual_at' => Yii::t('app', 'Start Actual At'),
            'end_actual_at' => Yii::t('app', 'End Actual At'),
            'status' => Yii::t('app', 'Status'),
            'file' => Yii::t('app', 'File'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->start_at = strtotime($this->start_at);
            $this->end_at = strtotime($this->end_at);
            return true;
        } else {
            return false;
        }
    }

    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_notulen']);
    }

    public function validateDate($attribute, $params, $validator) {
        if (strtotime($this->end_at) < strtotime($this->start_at)) {
            $this->addError($attribute, Yii::t('app', 'End time smaller than start time'));
        }
    }

}
