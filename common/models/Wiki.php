<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wiki".
 *
 * @property int $id
 * @property int $id_project
 * @property string $title
 * @property string $content
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Wiki extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'wiki';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_project', 'title', 'status', 'created_at', 'updated_at'], 'required'],
            [['id_project', 'status', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_project' => Yii::t('app', 'Project'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Last Updated'),
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $author = new WikiUser();
        $author->id_wiki = $this->id;
        $author->id_user = Yii::$app->user->identity->id;
        $author->created_at = time();
        $author->updated_at = time();
        $author->save();
    }

    public function getHistories() {
        return $this->hasMany(WikiUser::className(), ['id_wiki' => 'id']);
    }

    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }

}
