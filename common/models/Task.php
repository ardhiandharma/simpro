<?php

namespace common\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $id_project
 * @property int $id_sub_task
 * @property string $name
 * @property string $description
 * @property int $start_plan_at
 * @property int $end_plan_at
 * @property int $start_actual_at
 * @property int $end_actual_at
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $id_user 
 * @property Project $project
 */
class Task extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_project', 'name', 'start_plan_at', 'end_plan_at', 'status', 'created_at', 'updated_at'], 'required'],
            [['id_project', 'id_sub_task', 'start_actual_at', 'end_actual_at', 'created_at', 'updated_at', 'id_user'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 10],
            [['end_plan_at'], function($attribute, $params, $validato) {
                    if (strtotime($this->end_plan_at) < strtotime($this->start_plan_at)) {
                        $this->addError($attribute, Yii::t('app', 'Deadline smaller than start date'));
                    }
                }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_project' => Yii::t('app', 'Project'),
            'id_sub_task' => Yii::t('app', 'Parent Task'),
            'name' => Yii::t('app', 'Task'),
            'description' => Yii::t('app', 'Description'),
            'start_plan_at' => Yii::t('app', 'Start Plan At'),
            'end_plan_at' => Yii::t('app', 'Deadline'),
            'start_actual_at' => Yii::t('app', 'Start Actual At'),
            'end_actual_at' => Yii::t('app', 'End Actual At'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'id_user' => Yii::t('app', 'Responsible Person'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->start_plan_at = strtotime($this->start_plan_at);
            $this->end_plan_at = strtotime($this->end_plan_at);
            return true;
        } else {
            return false;
        }
    }

    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getParent() {
        return $this->hasOne(Task::className(), ['id' => 'id_sub_task']);
    }

    public function getChildrens() {
        return $this->hasMany(Task::className(), ['id_sub_task' => 'id']);
    }

}
