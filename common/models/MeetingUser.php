<?php

namespace common\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "meeting_user".
 *
 * @property int $id
 * @property int $id_meeting
 * @property int $id_user
 * @property int $created_at
 * @property int $updated_at
 */
class MeetingUser extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'meeting_user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_meeting', 'id_user', 'created_at', 'updated_at'], 'required'],
            [['id_meeting', 'id_user', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_meeting' => Yii::t('app', 'Id Meeting'),
            'id_user' => Yii::t('app', 'Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getMeeting() {
        return $this->hasOne(Meeting::className(), ['id' => 'id_meeting']);
    }

}
