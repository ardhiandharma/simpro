<?php

namespace common\models;

use Yii;
use dektrium\user\models\User;
/**
 * This is the model class for table "project_user".
 *
 * @property int $id
 * @property int $id_project
 * @property int $id_user
 * @property int $created_at
 * @property int $updated_at
 * @property ProjectUser $project
 * @property User $user
 */
class ProjectUser extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'project_user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_project', 'id_user', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_project' => Yii::t('app', 'Project'),
            'id_user' => Yii::t('app', 'User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

}
