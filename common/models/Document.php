<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property double $size
 * @property string $type
 * @property string $filename
 * @property int $created_at
 * @property int $updated_at
 * @property int $id_project
 */
class Document extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'document';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'name', 'filename', 'created_at', 'updated_at', 'id_project'], 'required'],
            [['id_user', 'created_at', 'updated_at', 'id_project'], 'integer'],
            [['size'], 'number'],
            [['name', 'type', 'filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'name' => Yii::t('app', 'Name'),
            'size' => Yii::t('app', 'Size'),
            'type' => Yii::t('app', 'Type'),
            'filename' => Yii::t('app', 'Filename'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'id_project' => Yii::t('app', 'Id Project'),
        ];
    }

    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }

}
