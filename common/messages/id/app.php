<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Add Project Member' => '',
    'Agenda' => '',
    'Are you sure you want to delete this item?' => '',
    'Author' => '',
    'Availibility' => '',
    'Content' => '',
    'Coordiator' => '',
    'Create Discussion' => '',
    'Create Document' => '',
    'Create Meeting' => '',
    'Create Project' => '',
    'Create Task' => '',
    'Created At' => '',
    'Created By' => '',
    'Deadline' => '',
    'Delete' => '',
    'Description' => '',
    'Details' => '',
    'Discussion' => '',
    'Discussions' => '',
    'Document' => '',
    'Documents' => '',
    'Done' => '',
    'Drive' => '',
    'End Actual At' => '',
    'End At' => '',
    'Entity' => '',
    'Entity ID' => '',
    'Filename' => '',
    'ID' => '',
    'Id Meeting' => '',
    'Id Project' => '',
    'Id Task' => '',
    'Id User' => '',
    'Level' => '',
    'Location' => '',
    'Login' => '',
    'Logout' => '',
    'Meeting' => '',
    'Meetings' => '',
    'Members' => '',
    'Message' => '',
    'Name' => '',
    'Notulen' => '',
    'On Going' => '',
    'Parent ID' => '',
    'Parent Task' => '',
    'Participants' => '',
    'Planned' => '',
    'Project' => '',
    'Project Members' => '',
    'Project Users' => '',
    'Projects' => '',
    'Related To' => '',
    'Replies' => '',
    'Reset' => '',
    'Responsible Person' => '',
    'SIMPRO' => '',
    'Save' => '',
    'Search' => '',
    'Select Member' => '',
    'Select Notulen' => '',
    'Select Parent Task' => '',
    'Select Responsible Person' => '',
    'Select Room' => '',
    'Size' => '',
    'Start' => '',
    'Start Actual At' => '',
    'Start At' => '',
    'Start Dates' => '',
    'Start Plan At' => '',
    'Status' => '',
    'Subject' => '',
    'Summary' => '',
    'Task' => '',
    'Tasks' => '',
    'The requested page does not exist.' => '',
    'Total Tasks' => '',
    'Type' => '',
    'Update' => '',
    'Update Discussion: {nameAttribute}' => '',
    'Update Document: {nameAttribute}' => '',
    'Update Meeting: {nameAttribute}' => '',
    'Update Project User: {nameAttribute}' => '',
    'Update Project: {nameAttribute}' => '',
    'Update Task: {nameAttribute}' => '',
    'Updated At' => '',
    'Updated By' => '',
    'Url' => '',
    'Wiki' => '',
    'Workload' => '',
    'Forgot password?' => 'Lupa kata sandi',
    'Home' => 'Beranda',
    'Main Menu' => 'Menu Utama',
    'Password' => 'Kata Sandi',
    'Profile' => 'Profil',
    'Sign in' => 'Masuk',
    'Sign out' => 'Keluar',
    'User Management' => 'Manajemen Pengguna',
];
