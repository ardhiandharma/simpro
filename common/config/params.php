<?php

return [
    'adminEmail' => 'adyhandoyo@fiskal.depkeu.go.id',
    'supportEmail' => 'adyhandoyo@fiskal.depkeu.go.id',
    'user.passwordResetTokenExpire' => 3600,
];
