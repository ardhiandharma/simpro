<?php

namespace common\components;

use Yii;
use common\models\Meeting;
use common\models\Task;
use common\models\Project;
use machour\yii2\notifications\models\Notification as BaseNotification;

class Notification extends BaseNotification {

    /**
     * A new message notification
     */
    const KEY_NEW_TASK = 'new_task';

    /**
     * A meeting reminder notification
     */
    const KEY_MEETING_REMINDER = 'meeting_reminder';

    /**
     * No disk space left !
     */
    const KEY_NEW_PROJECT = 'new_project';

    /**
     * @var array Holds all usable notifications
     */
    public static $keys = [
        self::KEY_NEW_TASK,
        self::KEY_MEETING_REMINDER,
        self::KEY_NEW_PROJECT,
    ];

    /**
     * @inheritdoc
     */
    public function getTitle() {
        switch ($this->key) {
            case self::KEY_MEETING_REMINDER:
                return Yii::t('app', 'Meeting reminder');

            case self::KEY_NEW_TASK:
                return Yii::t('app', 'You got a new task');

            case self::KEY_NEW_PROJECT:
                return Yii::t('app', 'You got a new project');
        }
    }

    /**
     * @inheritdoc
     */
    public function getDescription() {
        switch ($this->key) {
            case self::KEY_MEETING_REMINDER:
                $meeting = Meeting::findOne($this->key_id);
                return Yii::t('app', 'You are invited to {meeting}', [
                            'meeting' => $meeting->name
                ]);

            case self::KEY_NEW_TASK:
                $task = Task::findOne($this->key_id);
                return Yii::t('app', 'You are assigned on {task}', [
                            'task' => $task->name
                ]);

            case self::KEY_NEW_PROJECT:
                $project = Project::findOne($this->key_id);
                return Yii::t('app', 'You are added to {project}', [
                            'project' => $project->name
                ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function getRoute() {
        switch ($this->key) {
            case self::KEY_MEETING_REMINDER:
                return ['/meeting/view', 'id' => $this->key_id];

            case self::KEY_NEW_TASK:
                return ['/task-own/view', 'id' => $this->key_id];

            case self::KEY_NEW_PROJECT:
                return ['/project/view', 'id' => $this->key_id];
                ;
        };
    }

}
