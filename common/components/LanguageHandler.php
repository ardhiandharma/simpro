<?php

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\web\Application;

/**
 * Description of LanguageHandler
 *
 * @author lenovo
 */
class LanguageHandler extends Behavior {

    public function events() {
        return[Application::EVENT_BEFORE_REQUEST => 'changeLanguage'];
    }

    public function changeLanguage() {
        if (Yii::$app->getRequest()->getCookies()->has('language')) {
            Yii::$app->language = Yii::$app->getRequest()->getCookies()->getValue('language');
        }
    }

}
