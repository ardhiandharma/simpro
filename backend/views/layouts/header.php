<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$initial = ucwords(substr(Yii::$app->user->identity->username, 0, 1));
?>

<header class="main-header">
    <?= Html::a('<span class="logo-mini"><i class="fa fa-laptop"></i></span><span class="logo-lg">Dashboard</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="http://dummyimage.com/160x160/000/fff&text=<?= $initial ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="http://dummyimage.com/160x160/000/fff&text=<?= $initial ?>" class="img-circle"
                                 alt="User Image"/>
                            <p>
                                <?= Yii::$app->user->identity->username . '<br/>' . Yii::$app->user->identity->email; ?>
                                <small>Member since <?= date('M Y', Yii::$app->user->identity->created_at); ?></small>
                            </p>
                        </li>                        
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?=
                                Html::a(Yii::t('app', 'Profile'), ['/user/profile/show', 'id' => Yii::$app->user->identity->id], ['class' => 'btn btn-default btn-flat'])
                                ?>
                            </div>
                            <div class="pull-right">
                                <?=
                                Html::a(Yii::t('app', 'Sign out'), ['/user/security/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat'])
                                ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <!--li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li-->
            </ul>
        </div>
    </nav>
</header>
