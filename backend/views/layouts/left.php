<?php

use yii\bootstrap\Nav;

$initial = ucwords(substr(Yii::$app->user->identity->username, 0, 1));
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="http://dummyimage.com/160x160/000/fff&text=<?= $initial ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?=
        Nav::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        '<li class="header">' . Yii::t('app', 'Main Menu') . '</li>',
                        ['label' => '<i class="fa fa-home"></i><span>' . Yii::t('app', 'Home') . '</span>', 'url' => ['/site/index']],
                        ['label' => '<i class="fa fa-user"></i><span>' . Yii::t('app', 'User Management') . '</span>', 'url' => ['/user/admin/index']],
                        ['label' => '<i class="fa fa-file-code-o"></i><span>Gii</span>', 'url' => ['/gii'], 'linkOptions' => ['target' => '_blank']],
                        ['label' => '<i class="fa fa-dashboard"></i><span>Debug</span>', 'url' => ['/debug'], 'linkOptions' => ['target' => '_blank']],
                        [
                            'label' => '<i class="glyphicon glyphicon-lock"></i><span>Sign in</span>', //for basic
                            'url' => ['/user/security/login'],
                            'visible' => Yii::$app->user->isGuest
                        ],
                    ],
                ]
        );
        ?>
    </section>
</aside>
