<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Project;
use common\models\ProjectUser;

/**
 * ProjectSearch represents the model behind the search form of `common\models\Project`.
 */
class ProjectSearch extends Project {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'id_user', 'start_plan_at', 'end_plan_at', 'start_actual_at', 'end_actual_at', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $ids = [];
        foreach (Project::find()->where(['id_user' => Yii::$app->user->identity->id])->all() as $project) {
            $ids[] = $project->id;
        }
        foreach (ProjectUser::find()->where(['id_user' => Yii::$app->user->identity->id])->all() as $value) {
            $ids[] = $value->id_project;
        }

        $query = Project::find()->where(['in', 'id', $ids]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'start_plan_at' => $this->start_plan_at,
            'end_plan_at' => $this->end_plan_at,
            'start_actual_at' => $this->start_actual_at,
            'end_actual_at' => $this->end_actual_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->orFilterWhere(['like', 'description', $this->name])
                ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

}
