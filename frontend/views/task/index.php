<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tabs\TabsX;
use common\models\Project;
use professionalweb\ScheduleWidget\ScheduleWidget;
use yii\web\JsExpression;
use professionalweb\ScheduleWidget\wrappers\Row;
use professionalweb\ScheduleWidget\wrappers\Task;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modelsTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tasks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = $this->title;

$project = Project::find()->where(['id' => $id_project])->one();

$data = [];
$counter = 0;
foreach ($dataProvider->models as $model) {
    $counter++;
    $row = new Row();
    $row->name = Yii::t('app', 'Task') . ' ' . $counter;
    $c = [];
    foreach ($model->childrens as $children) {
        $c[] = $children->name;
    }
    $row->children = $c;
    if (!is_null($model->id_sub_task)) {
        $row->parent = $model->parent->name;
    }
    $task = new Task();
    $task->name = $model->name;
    $task->from = date('Y-m-d', $model->start_plan_at);
    $task->to = date('Y-m-d', $model->end_plan_at);
    $task->classes = ['event-row'];
    $row->tasks[] = $task;
    $data[] = $row;
}

//var_dump(Json::encode($data));
//exit();
?>
<div class="task-index">
    <h1><?= Html::encode($project->name) ?></h1>

    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Task'),
                'url' => ['task/project/' . $id_project],
                'active' => true
            ],
            [
                'label' => Yii::t('app', 'Meeting'),
                'url' => ['meeting/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Document'),
                'url' => ['document/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Wiki'),
                'url' => ['wiki/project/' . $id_project],
            ],
        ],
    ]);
    ?>

    <?php
    echo ScheduleWidget::widget([
        'clientOptions' => [
            'daily' => 'true',
            'column-magnet' => 'column',
            'time-frames-magnet' => false
        ],
        'plugins' => [
            ScheduleWidget::PLUGIN_MOVABLE => [],
            ScheduleWidget::PLUGIN_TABLE => [
                'headers' => [
                    'model.name' => 'Name'
                ]
            ],
            ScheduleWidget::PLUGIN_TOOLTIP => []
        ],
        'events' => [
            ScheduleWidget::EVENT_TASK_MOVEEND => new JsExpression('function(task){'
                    . 'console.log(task.row.model);'
                    . '}')
        ],
        'data' => Json::encode($data)
    ]);
    ?>
    <br/>
    <?php if ($project->id_user == Yii::$app->user->identity->id): ?>
        <p>
            <?= Html::a(Yii::t('app', 'Add'), ['task/project/' . $id_project . '/create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_user',
                'format' => 'raw',
                'value' => function ($model) {
                    return is_null($model->id_user) ? '' : $model->user->profile->name;
                }
            ],
            [
                'attribute' => 'name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Search here')
                ]
            ],
            'end_plan_at:datetime',
            'status',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);
    ?>
</div>
