<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tasks'), 'url' => ['index', 'id_project' => $model->id_project]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?php if (is_null($model->start_actual_at) && $model->project->id_user == Yii::$app->user->identity->id): ?>
            <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        <?php endif; ?>
        <?php if (is_null($model->start_actual_at) && $model->id_user == Yii::$app->user->identity->id): ?>
            <?=
            Html::a(Yii::t('app', 'Start'), ['start', 'id' => $model->id], [
                'class' => is_null($model->project->start_actual_at) ? 'btn btn-success disabled' : 'btn btn-success',
            ])
            ?>
        <?php elseif (is_null($model->end_actual_at) && $model->id_user == Yii::$app->user->identity->id): ?>
            <?= Html::a(Yii::t('app', 'Finish'), ['start', 'id' => $model->id, 'flag' => FALSE], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </p>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_user',
                'value' => is_null($model->id_user) ? '' : $model->user->profile->name
            ],
            'id_sub_task',
            'name',
            'description:html',
            'start_plan_at:datetime',
            'end_plan_at:datetime',
            'status',
        ],
    ])
    ?>

</div>
