<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\Task;
use dektrium\user\models\Profile;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Progress;
use yii\bootstrap\Modal;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">
    <?php
    Modal::begin([
        'header' => '<h2>' . Yii::t('app', 'Availibility') . '</h2>',
        'toggleButton' => ['label' => Yii::t('app', 'Availibility'), 'class' => 'btn btn-warning btn-sm'],
    ]);
    foreach (Profile::find()->where(['in', 'user_id', $model->project->memberIds])->all() as $value):
        ?>
        <div class="row">
            <div class="col-xs-4">
                <?= $value->name ?>
            </div>
            <div class="col-xs-4">
                <?= Yii::t('app', 'Availibility') ?>
                <?php
                $rand = rand(0, 100);
                $class = '';
                if ($rand <= 33) {
                    $class = 'progress-bar-success';
                } elseif ($rand <= 66) {
                    $class = 'progress-bar-warning';
                } else {
                    $class = 'progress-bar-danger';
                }
                echo Progress::widget([
                    'percent' => $rand,
                    'barOptions' => ['class' => $class],
                    'options' => ['class' => 'active progress-striped']
                ]);
                ?>
            </div>
            <div class = "col-xs-4">
                <?= Yii::t('app', 'Workload') ?> 
                <?php
                $rand = rand(0, 100);
                $class = '';
                if ($rand <= 33) {
                    $class = 'progress-bar-success';
                } elseif ($rand <= 66) {
                    $class = 'progress-bar-warning';
                } else {
                    $class = 'progress-bar-danger';
                }
                echo Progress::widget([
                    'percent' => $rand,
                    'barOptions' => ['class' => $class],
                    'options' => ['class' => 'active progress-striped']
                ]);
                ?>
            </div>
        </div>
        <?php
    endforeach;
    Modal::end();
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user')->dropDownList(ArrayHelper::map(Profile::find()->where(['in', 'user_id', $model->project->memberIds])->all(), 'user_id', 'name'), ['prompt' => Yii::t('app', 'Select Responsible Person')]) ?>

    <?= $form->field($model, 'id_project', ['options' => ['class' => 'hide']])->hiddenInput() ?>

    <?= $form->field($model, 'id_sub_task')->dropDownList(ArrayHelper::map(Task::find()->where(['id_project' => $model->id_project])->all(), 'id', 'name'), ['prompt' => Yii::t('app', 'Select Parent Task')]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            'toolbar' => 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'start_plan_at')->widget(DateTimePicker::className(), [
        'options' => ['placeholder' => Yii::t('app', 'Select start date')],
        'pluginOptions' => [
            'format' => 'dd-M-yyyy hh:ii',
            'todayHighlight' => true,
            'daysOfWeekDisabled' => [0, 6],
            'startDate' => date('d-M-Y H:i'),
        ]
    ])
    ?>

    <?=
    $form->field($model, 'end_plan_at')->widget(DateTimePicker::className(), [
        'options' => ['placeholder' => Yii::t('app', 'Select end date')],
        'pluginOptions' => [
            'format' => 'dd-M-yyyy hh:ii',
            'todayHighlight' => true,
            'daysOfWeekDisabled' => [0, 6],
            'startDate' => date('d-M-Y H:i'),
        ]
    ])
    ?>

    <?= $form->field($model, 'status', ['options' => ['class' => 'hide']])->hiddenInput(['value' => Yii::t('app', 'Planned')]) ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'created_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>                                                                                                                                                                                                

</div>
