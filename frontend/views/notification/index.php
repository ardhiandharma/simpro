<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Notification;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Notifications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => Yii::t('app', 'Name'),
                'value' => function ($model) {
                    $url = ['meeting/view', 'id' => $model->key_id];
                    $name = Yii::t('app', 'Meeting reminder');
                    if ($model->key == Notification::KEY_NEW_TASK) {
                        $url = ['task-own/view', 'id' => $model->key_id];
                        $name = Yii::t('app', 'You got a new task');
                    } elseif ($model->key == Notification::KEY_NEW_PROJECT) {
                        $url = ['project/view', 'id' => $model->key_id];
                        $name = Yii::t('app', 'You got a new project');
                    }
                    return Html::a($name, $url);
                },
                'format' => 'raw'
            ],
            'created_at',
        ],
    ]);
    ?>
</div>
