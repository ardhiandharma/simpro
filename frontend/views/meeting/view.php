<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Meetings'), 'url' => ['meeting/project/' . $model->project->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Meeting'),
                'url' => ['view', 'id' => $model->id],
                'active' => true
            ],
            [
                'label' => Yii::t('app', 'Participant'),
                'url' => ['meeting-user/meeting/' . $model->id],
            ],
        ],
    ]);
    ?>
    <p>
        <?php if ($model->project->id_user == Yii::$app->user->identity->id): ?>
            <?php if (is_null($model->start_actual_at)): ?>
                <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?=
                Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
                <?= Html::a(Yii::t('app', 'Start'), ['start', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?php elseif (is_null($model->end_actual_at)): ?>
                <?= Html::a(Yii::t('app', 'Finish'), ['start', 'id' => $model->id, 'flag' => FALSE], ['class' => 'btn btn-danger']) ?>
            <?php endif; ?>
        <?php endif; ?>
        <?php if (!is_null($model->start_actual_at) && $model->id_notulen == Yii::$app->user->identity->id): ?>
            <?= Html::a(Yii::t('app', 'Notulen'), ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?php endif; ?>
    </p>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'agenda:html',
            'start_at:datetime',
            'end_at:datetime',
            'summary:html',
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => function($model) {
                    return is_null($model->file) ? '' : Html::a(Yii::t('app', 'Download') . ' <span class="glyphicon glyphicon-save"></span>', ['file', 'id' => $model->id], ['style' => 'text-decoration: none', 'target' => '_blank']);
                }
            ],
            [
                'label' => Yii::t('app', 'Notulen'),
                'value' => is_null($model->id_notulen) ? '' : $model->user->profile->name
            ]
        ],
    ])
    ?>

</div>
