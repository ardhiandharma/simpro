<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tabs\TabsX;
use common\models\Project;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MeetingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Meetings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = $this->title;
$project = Project::find()->where(['id' => $id_project])->one();
?>
<div class="meeting-index">
    <h1><?= Html::encode($project->name) ?></h1>
    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Task'),
                'url' => ['task/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Meeting'),
                'url' => ['meeting/project/' . $id_project],
                'active' => true
            ],
            [
                'label' => Yii::t('app', 'Document'),
                'url' => ['document/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Wiki'),
                'url' => ['wiki/project/' . $id_project],
            ],
        ],
    ]);
    ?>
    <?php if ($project->id_user == Yii::$app->user->identity->id): ?>
        <p>
            <?= Html::a(Yii::t('app', 'Add'), ['meeting/project/' . $id_project . '/create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Search here')
                ]
            ],
            'start_at:datetime',
            [
                'attribute' => 'id_notulen',
                'value' => function ($model) {
                    return is_null($model->id_notulen) ? '' : $model->user->profile->name;
                }
            ],
            'location',
            'status',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]);
    ?>
</div>
