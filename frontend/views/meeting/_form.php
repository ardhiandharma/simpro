<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dektrium\user\models\Profile;
use dosamigos\tinymce\TinyMce;
use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?php if ($model->project->id_user == Yii::$app->user->identity->id && is_null($model->start_actual_at)): ?>
        <?= $form->field($model, 'id_project', ['options' => ['class' => 'hide']])->hiddenInput() ?>

        <?= $form->field($model, 'id_notulen')->dropDownList(ArrayHelper::map(Profile::find()->where(['in', 'user_id', $model->project->memberIds])->all(), 'user_id', 'name'), ['prompt' => Yii::t('app', 'Select Notulen')]) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?=
        $form->field($model, 'agenda')->widget(TinyMce::className(), [
            'options' => ['rows' => 6],
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                'toolbar' => 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            ]
        ]);
        ?>

        <?= $form->field($model, 'location')->dropDownList(['Ruang 1' => 'Ruang 1', 'Ruang 2' => 'Ruang 2'], ['prompt' => Yii::t('app', 'Select Room')]) ?>

        <?=
        $form->field($model, 'start_at')->widget(DateTimePicker::className(), [
            'options' => ['placeholder' => Yii::t('app', 'Select start time')],
            'pluginOptions' => [
                'format' => 'dd-M-yyyy hh:ii',
                'todayHighlight' => true,
                'daysOfWeekDisabled' => [0, 6],
                'startDate' => date('d-M-Y H:i'),
            ]
        ])
        ?>

        <?=
        $form->field($model, 'end_at')->widget(DateTimePicker::className(), [
            'options' => ['placeholder' => Yii::t('app', 'Select start time')],
            'pluginOptions' => [
                'format' => 'dd-M-yyyy hh:ii',
                'todayHighlight' => true,
                'daysOfWeekDisabled' => [0, 6],
                'startDate' => date('d-M-Y H:i'),
            ]
        ])
        ?>
    <?php endif; ?>

    <?php if ($model->id_notulen == Yii::$app->user->identity->id && !is_null($model->start_actual_at)): ?>
        <?=
        $form->field($model, 'summary')->widget(TinyMce::className(), [
            'options' => ['rows' => 6],
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                'toolbar' => 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            ]
        ]);
        ?>

        <?=
        $form->field($model, 'file')->widget(FileInput::classname(), [
            'options' => [
                'accept' => 'application/pdf'
            ],
        ])
        ?>
    <?php endif; ?>
    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'status', ['options' => ['class' => 'hide']])->hiddenInput(['value' => Yii::t('app', 'Planned')]) ?>
        <?= $form->field($model, 'created_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
