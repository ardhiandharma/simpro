<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Discussion */

$this->title = Yii::t('app', 'Create Discussion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discussions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discussion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
