<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Discussion */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discussions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discussion-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => Yii::t('app', 'Author'),
                'value' => $model->user->profile->name
            ],
            [
                'label' => Yii::t('app', 'Project'),
                'value' => $model->project->name
            ],
            'message:html',
        ],
    ])
    ?>

    <?=
    \yii2mod\comments\widgets\Comment::widget([
        'model' => $model,
        'dataProviderConfig' => [
            'pagination' => [
                'pageSize' => 10
            ],
        ]
    ]);
    ?>

</div>
