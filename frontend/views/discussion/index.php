<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DiscussionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Discussions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discussion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_project',
                'label' => Yii::t('app', 'Project'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->project->name;
                }
            ],
            [
                'attribute' => 'subject',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Search here')
                ]
            ], [
                'attribute' => 'id_user',
                'label' => Yii::t('app', 'Author'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->user->profile->name;
                }
            ],
            [
                'attribute' => 'id_user',
                'label' => Yii::t('app', 'Replies'),
                'format' => 'raw',
                'value' => function ($model) {
                    return count($model->comments);
                }
            ],
            [
                'format' => 'html',
                'value' => function($model) {
                    $buttons = Html::a('<i class="glyphicon glyphicon-eye-open"></i>', ['view', 'id' => $model->id]);
                    if ($model->id_user == Yii::$app->user->identity->id) {
                        $buttons .= ' ' . Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $model->id])
                                . ' ' . Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                    'data' => ['confirm' => 'Do you really want to delete this element?', 'method' => 'post']
                                        ]
                        );
                    }
                    return $buttons;
                },
            ],
        ],
    ]);
    ?>
</div>
