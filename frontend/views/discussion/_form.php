<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Project;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Discussion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discussion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user', ['options' => ['class' => 'hide']])->hiddenInput(['value' => Yii::$app->user->identity->id]) ?>

    <?= $form->field($model, 'id_project')->dropDownList(ArrayHelper::map(Project::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'message')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            'toolbar' => 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        ]
    ]);
    ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'created_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
