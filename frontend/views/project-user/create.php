<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectUser */

$this->title = Yii::t('app', 'Add Project Member');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Users'), 'url' => ['index', 'id_project' => $id_project]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
