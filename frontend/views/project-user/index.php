<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tabs\TabsX;
use common\models\Project;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProjectUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Project Members');
$this->params['breadcrumbs'][] = $this->title;
$project = Project::find()->where(['id' => $id_project])->one();
?>
<div class="project-user-index">
    <h1><?= Html::encode($project->name) ?></h1>
    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Details'),
                'url' => ['project/view', 'id' => $id_project],
            ],
            [
                'label' => Yii::t('app', 'Members'),
                'url' => ['project-user/project/' . $id_project],
                'active' => true
            ],
        ],
    ]);
    ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
    <?php if ($id_leader == Yii::$app->user->identity->id): ?>
        <p>
            <?= Html::a(Yii::t('app', 'Add'), ['project-user/project/' . $id_project . '/create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_user',
                'label' => Yii::t('app', 'Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->user->profile->name;
                }
            ],
            [
                'format' => 'html',
                'value' => function($model) {
                    if ($model->project->id_user == Yii::$app->user->identity->id && is_null($model->project->start_actual_at)) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                    'data' => ['confirm' => 'Do you really want to delete this element?', 'method' => 'post']
                                        ]
                        );
                    }
                    return '';
                },
            ],
        ],
    ]);
    ?>
</div>
