<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\WikiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wiki');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_project',
                'format' => 'raw',
                'value' => function($model) {
                    return$model->project->name;
                }
            ],
            [
                'attribute' => 'title',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Search here')
                ]
            ], 'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a("<span class='glyphicon glyphicon-eye-open'></span>", ['timeline', 'id' => $model->id]);
                    }
                ]
            ],
        ],
    ]);
    ?>
</div>
