<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Wiki */

$this->title = Yii::t('app', 'Create Wiki');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wiki'), 'url' => ['index', 'id_project' => $model->id_project]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
