<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Wiki */

$this->title = Yii::t('app', 'Update Wiki: {nameAttribute}', [
            'nameAttribute' => $model->title,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wiki'), 'url' => ['index', 'id_project' => $model->id_project]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="wiki-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
