<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Wiki */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_project', ['options' => ['class' => 'hide']])->hiddenInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'content')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            'toolbar' => 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        ]
    ]);
    ?>
    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'created_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>

    <?= $form->field($model, 'status', ['options' => ['class' => 'hide']])->hiddenInput(['value' => 0]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
