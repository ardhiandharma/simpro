<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Project;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\WikiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wiki');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = $this->title;
$project = Project::find()->where(['id' => $id_project])->one();
?>
<div class="wiki-index">

    <h1><?= Html::encode($project->name) ?></h1>

    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Task'),
                'url' => ['task/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Meeting'),
                'url' => ['meeting/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Document'),
                'url' => ['document/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Wiki'),
                'url' => ['wiki/project/' . $id_project],
                'active' => true
            ],
        ],
    ]);
    ?>
    <?php if (count($dataProvider->models) <= 0): ?>
        <p>
            <?= Html::a(Yii::t('app', 'Add'), ['create', 'id_project' => $id_project], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Search here')
                ]
            ], 'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]);
    ?>
</div>
