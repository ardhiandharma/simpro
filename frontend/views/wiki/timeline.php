<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use vivekmarakana\widgets\Timeline;

/* @var $this yii\web\View */
/* @var $model common\models\Wiki */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wiki'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-xs-8">
            <p class="text-muted"><?= Yii::t('app', 'Created') . ': ' . date('d-m-Y', $model->created_at) . ' | ' . Yii::t('app', 'Edited') . ': ' . count($model->histories) . 'x' ?></p>
            <hr/>
            <?= $model->content ?>
            <h3><?= Yii::t('app', 'Log') ?></h3>
            <table class="table table-responsive table-bordered">
                <thead>
                    <tr>
                        <th><?= Yii::t('app', 'Name') ?></th>
                        <th><?= Yii::t('app', 'Last Updated') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($model->histories as $history): ?>
                        <tr>
                            <td><?= $history->user->profile->name ?></td>
                            <td><?= date('d-M-Y H:i', $history->created_at) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-xs-4">
            <?php
            $items = [];
            foreach ($model->project->tasks as $post) {
                $data = [];
                $data['title'] = $post->name;
                $data['date'] = date('d-m-Y', $post->start_plan_at);
                $data['time'] = date('H:i', $post->start_plan_at);
//                $data['notes'] = $post->description;
                $items[] = $data;
            }
            echo Timeline::widget(['items' => $items]);
            ?>
        </div>
    </div> 
</div>
