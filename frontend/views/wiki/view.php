<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Wiki */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wiki'), 'url' => ['index', 'id_project' => $model->id_project]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if ($model->project->id_user == Yii::$app->user->identity->id && $model->status != 1): ?>
            <?= Html::a(Yii::t('app', 'Approve'), ['approve', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        <?php endif; ?>
    </p>
    <p class="text-muted"><?= Yii::t('app', 'Created') . ': ' . date('d-m-Y', $model->created_at) . ' | ' . Yii::t('app', 'Edited') . ': ' . count($model->histories) . 'x' ?></p>
    <hr/>
    <?= $model->content ?>    <h3><?= Yii::t('app', 'Log') ?></h3>
    <table class="table table-responsive table-bordered">
        <thead>
            <tr>
                <th><?= Yii::t('app', 'Name') ?></th>
                <th><?= Yii::t('app', 'Last Updated') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model->histories as $history): ?>
                <tr>
                    <td><?= $history->user->profile->name ?></td>
                    <td><?= date('d-M-Y H:i', $history->created_at) ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
