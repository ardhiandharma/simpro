<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingUser */

$this->title = Yii::t('app', 'Create Meeting Participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Meetings'), 'url' => ['meeting/view', 'id' => $id_meeting]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Meeting Participants'), 'url' => ['index', 'id_meeting' => $id_meeting]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
