<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MeetingUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Meeting Participants');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Meetings'), 'url' => ['meeting/view', 'id' => $id_meeting]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-user-index">
    <h1><?= Html::encode($meeting->name) ?></h1>
    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Meeting'),
                'url' => ['meeting/view', 'id' => $id_meeting],
            ],
            [
                'label' => Yii::t('app', 'Participant'),
                'url' => ['meeting-user/meeting/' . $id_meeting],
                'active' => true
            ],
        ],
    ]);
    ?>

    <p>
        <?php if (is_null($meeting->end_actual_at) && $meeting->project->id_user == Yii::$app->user->identity->id): ?>
            <?= Html::a(Yii::t('app', 'Add'), ['meeting-user/meeting/' . $id_meeting . '/create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_user',
                'value' => function ($model) {
                    return $model->user->profile->name;
                }
            ],
            [
                'format' => 'html',
                'value' => function($model) {
                    if ($model->meeting->project->id_user == Yii::$app->user->identity->id && is_null($model->meeting->start_actual_at)) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                    'data' => ['confirm' => 'Do you really want to delete this element?', 'method' => 'post']
                                        ]
                        );
                    }
                    return '';
                },
            ],
        ],
    ]);
    ?>
</div>
