<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dektrium\user\models\Profile;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_meeting', ['options' => ['class' => 'hide']])->hiddenInput() ?>

    <?= $form->field($model, 'id_user')->dropDownList(ArrayHelper::map(Profile::find()->all(), 'user_id', 'name'), ['prompt' => Yii::t('app', 'Select Participant')]) ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'created_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
