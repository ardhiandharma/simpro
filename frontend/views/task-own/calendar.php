<?php

use yii\helpers\Html;
use yii2fullcalendar\yii2fullcalendar;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TaskOwnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Calendar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-calendar">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    yii2fullcalendar::widget(array(
        'events' => $events,
        'header' => [
            'right' => 'month,agendaWeek,agendaDay',
            'center' => 'prevYear,prev,today,next,nextYear'
        ],
        'clientOptions' => [
            'displayEventTime' => FALSE,
            'nowIndicator' => TRUE
        ]
    ));
    ?>
</div>
