<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (is_null($model->start_actual_at) && $model->id_user == Yii::$app->user->identity->id): ?>
        <p>
            <?= Html::a(Yii::t('app', 'Start'), ['start', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </p>
    <?php elseif (is_null($model->end_actual_at) && $model->id_user == Yii::$app->user->identity->id): ?>
        <p>
            <?= Html::a(Yii::t('app', 'Done'), ['start', 'id' => $model->id, 'flag' => FALSE], ['class' => 'btn btn-danger']) ?>
        </p>
    <?php endif; ?>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'project.name',
            'name',
            'description:html',
            'start_plan_at:date',
            'end_plan_at:date',
            'status',
        ],
    ])
    ?>

</div>
