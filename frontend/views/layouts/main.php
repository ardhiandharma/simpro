<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use rmrevin\yii\favicon\Favicon;
use machour\yii2\notifications\widgets\NotificationsWidget;

raoul2000\bootswatch\BootswatchAsset::$theme = 'cosmo';
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?=
        Favicon::widget([
            'web' => '@web',
            'webroot' => '@webroot',
            'favicon' => '@webroot/favicon.png',
            'color' => '#2b5797',
            'viewComponent' => 'view',
        ]);
        ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::t('app', 'SIMPRO'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $menuItemsLeft = [];
            $menuItemsRight = [];
            if (Yii::$app->user->isGuest) {
                $menuItemsRight[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/user/security/login']];
            } else {
                $menuItemsLeft = [
                    ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
                    ['label' => Yii::t('app', 'Project'), 'url' => ['/project/index']],
                    [
                        'label' => Yii::t('app', 'Task'),
                        'items' => [
                            ['label' => Yii::t('app', 'My Tasks'), 'url' => ['/task-own/index']],
                            ['label' => Yii::t('app', 'Calendar'), 'url' => ['/task-own/calendar']],
                        ],
                    ],
                    ['label' => Yii::t('app', 'Discussion'), 'url' => ['/discussion/index']],
                    ['label' => Yii::t('app', 'Drive'), 'url' => ['/drive/index']],
                    ['label' => Yii::t('app', 'Wiki'), 'url' => ['/wiki']],
                ];

                NotificationsWidget::widget([
                    'theme' => NotificationsWidget::THEME_GROWL,
                    'clientOptions' => [
                        'location' => 'br',
                    ],
                    'counters' => [
                        '.notifications-header-count',
                        '.notifications-icon-count'
                    ],
                    'markAllSeenSelector' => '#notification-seen-all',
                    'listSelector' => '#notifications',
                ]);

                $menuItemsRight[] = [
                    'label' => '<i class="fa fa-bell-o"></i>  <span class="label label-danger notifications-icon-count">0</span>',
                    'items' => [
                        '<li class="header">You have <span class="notifications-header-count">0</span> notifications</li>',
                        '<li><ul class="menu"><div id="notifications"></div></ul></li>',
                        [
                            'label' => Html::a(Yii::t('app', 'View all'), ['notification/index']),
                            'options' => ['class' => 'footer']
                        ]
                    ],
                    'options' => ['class' => 'notifications-menu'],
                ];
                $menuItemsRight[] = [
                    'label' => Yii::$app->user->identity->username,
                    'items' => [
                        ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/settings/profile']],
                        [
                            'label' => Yii::t('app', 'Logout'),
                            'url' => ['/user/security/logout'],
                            'linkOptions' => ['data-method' => 'post']
                        ]
                    ],
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items' => $menuItemsLeft,
                'encodeLabels' => false
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItemsRight,
                'encodeLabels' => false
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Badan Kebijakan Fiskal <?= date('Y') ?></p>

                <p class="pull-right">Kementerian Keuangan</p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
