<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'id_user', ['options' => ['class' => 'hide']])->hiddenInput(['value' => Yii::$app->user->identity->id]) ?>

    <?= $form->field($model, 'id_project', ['options' => ['class' => 'hide']])->hiddenInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'filename')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*,application/pdf'
        ],
    ])
    ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'created_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
