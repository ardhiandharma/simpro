<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Project;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Documents');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = $this->title;
$project = Project::find()->where(['id' => $id_project])->one();
?>
<div class="document-index">
    <h1><?= Html::encode($project->name) ?></h1>

    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Task'),
                'url' => ['task/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Meeting'),
                'url' => ['meeting/project/' . $id_project],
            ],
            [
                'label' => Yii::t('app', 'Document'),
                'url' => ['document/project/' . $id_project],
                'active' => true
            ],
            [
                'label' => Yii::t('app', 'Wiki'),
                'url' => ['wiki/project/' . $id_project],
            ],
        ],
    ]);
    ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'), ['document/project/' . $id_project . '/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'created_at:datetime',
            'type',
            'size:size',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
            ],
        ],
    ]);
    ?>
</div>
