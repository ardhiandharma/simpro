<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user', ['options' => ['class' => 'hide']])->hiddenInput(['value' => Yii::$app->user->identity->id]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            'toolbar' => 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'start_plan_at')->widget(DatePicker::className(), [
        'options' => ['placeholder' => Yii::t('app', 'Select start date')],
        'pluginOptions' => [
            'format' => 'dd-M-yyyy',
            'todayHighlight' => true,
            'startDate' => date('d-M-Y'),
            'daysOfWeekDisabled' => [0, 6],
        ]
    ])
    ?>

    <?=
    $form->field($model, 'end_plan_at')->widget(DatePicker::className(), [
        'options' => ['placeholder' => Yii::t('app', 'Select end date')],
        'pluginOptions' => [
            'format' => 'dd-M-yyyy',
            'todayHighlight' => true,
            'daysOfWeekDisabled' => [0, 6],
            'startDate' => date('d-M-Y'),
        ]
    ])
    ?>

    <?= $form->field($model, 'status', ['options' => ['class' => 'hide']])->hiddenInput(['value' => Yii::t('app', 'Planned')]) ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'created_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'hide']])->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
