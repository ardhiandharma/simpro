<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_RIGHT,
        'sideways' => true,
        'items' => [
            [
                'label' => Yii::t('app', 'Details'),
                'url' => ['view', 'id' => $model->id],
                'active' => true
            ],
            [
                'label' => Yii::t('app', 'Members'),
                'url' => ['project-user/project/' . $model->id],
            ],
        ],
    ]);
    ?>
    <p>
        <?php if ($model->id_user == Yii::$app->user->identity->id): ?>
            <?php if (is_null($model->start_actual_at)): ?>
                <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?=
                Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => count($model->tasks) > 0 ? 'btn btn-danger disabled' : 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
                <?= Html::a(Yii::t('app', 'Start'), ['start', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?php elseif (is_null($model->end_actual_at)): ?>
                <?=
                Html::a(Yii::t('app', 'Finish'), ['start', 'id' => $model->id, 'flag' => FALSE], [
                    'class' => count($model->tasks) != count($model->completedTasks) ? 'btn btn-danger disabled' : 'btn btn-danger'
                ])
                ?>
            <?php endif; ?>
        <?php endif; ?>
    </p>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => Yii::t('app', 'Coordiator'),
                'attribute' => 'id_user',
                'value' => $model->user->profile->name,
            ],
            'name',
            'description:html',
            'start_plan_at:date',
            'end_plan_at:date',
            'status',
        ],
    ])
    ?>

</div>
