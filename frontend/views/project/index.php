<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modelsProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
//                    if (!is_null($model->start_actual_at)) {
                    return Html::a($model->name, ['task/project/' . $model->id]);
//                    } else {
//                        return $model->name;
//                    }
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Search here')
                ]
            ],
            'start_plan_at:date',
            'end_plan_at:date',
            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Ongoing Tasks'),
                'value' => function ($model) {
                    return count($model->onGoingTasks);
                },
            ],
            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Completed Tasks'),
                'value' => function ($model) {
                    return count($model->completedTasks);
                },
            ],
            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Total Tasks'),
                'value' => function ($model) {
                    return count($model->tasks);
                },
            ],
            'status',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]);
    ?>
</div>
