<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
+
$this->title = Yii::t('app', 'Drive');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
        <div class="col-xs-12">
            <?php
            echo alexantr\elfinder\ElFinder::widget([
                'connectorRoute' => ['connector'],
                'settings' => [
                    'height' => '500px',
                    'width' => '100%'
                ],
                'buttonNoConflict' => true,
            ])
            ?>
        </div>
    </div>
</div>
