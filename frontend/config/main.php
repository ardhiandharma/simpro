<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    // change the value based on URL 
    'homeUrl' => '/simpro',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'task/project/<id_project:\d+>' => 'task/index',
                'task/project/<id_project:\d+>/create' => 'task/create',
                'meeting/project/<id_project:\d+>' => 'meeting/index',
                'meeting/project/<id_project:\d+>/create' => 'meeting/create',
                'meeting/project/<id_project:\d+>/delete/<id:\d+>' => 'meeting/delete',
                'project-user/project/<id_project:\d+>' => 'project-user/index',
                'project-user/project/<id_project:\d+>/create' => 'project-user/create',
                'project-user/project/<id_project:\d+>/delete/<id:\d+>' => 'project-user/delete',
                'document/project/<id_project:\d+>' => 'document/index',
                'document/project/<id_project:\d+>/create' => 'document/create',
                'document/project/<id_project:\d+>/delete/<id:\d+>' => 'document/delete',
                'meeting-user/meeting/<id_meeting:\d+>' => 'meeting-user/index',
                'meeting-user/meeting/<id_meeting:\d+>/create' => 'meeting-user/create',
                'wiki/project/<id_project:\d+>' => 'wiki/index',
                'wiki/project/<id_project:\d+>/create' => 'wiki/create',
            ]
        ],
        'user' => [
            'identityCookie' => [
                'name' => '_frontendIdentity',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'FRONTENDSESSID_SIMPRO',
            'cookieParams' => [
                'httpOnly' => true,
            ],
        ],
        // change the value based on URL 
        'request' => [
            'baseUrl' => '/simpro',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        'fs' => [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            'path' => '@webroot/uploads',
        ],
    ],
    'modules' => [
        'user' => [
            // following line will restrict access to admin controller from frontend application
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
            'enableConfirmation' => false,
            'enableRegistration' => false,
        ],
        'comment' => [
            'class' => 'yii2mod\comments\Module',
        ],
    ],
    'as beforeRequest' => [
        'class' => 'common\components\LanguageHandler',
    ],
    'params' => $params,
];
