<?php

namespace frontend\controllers;

use Yii;
use common\models\Meeting;
use frontend\models\MeetingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MeetingController implements the CRUD actions for Meeting model.
 */
class MeetingController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Meeting models.
     * @return mixed
     */
    public function actionIndex($id_project) {
        $searchModel = new MeetingSearch();
        $searchModel->id_project = $id_project;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_project' => $id_project
        ]);
    }

    /**
     * Displays a single Meeting model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Meeting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_project) {
        $model = new Meeting();
        $model->id_project = $id_project;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
                    'id_project' => $id_project,
        ]);
    }

    /**
     * Updates an existing Meeting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->start_at = date('d-M-Y h:i', $model->start_at);
        $model->end_at = date('d-M-Y h:i', $model->end_at);
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if (!is_null($model->file)) {
                $path = $model->project->name . DIRECTORY_SEPARATOR . 'meeting' . DIRECTORY_SEPARATOR . time() . '_' . $model->file->name;
                if ($model->file->error === UPLOAD_ERR_OK) {
                    $stream = fopen($model->file->tempName, 'r+');
                    Yii::$app->fs->writeStream($path, $stream);
                    fclose($stream);
                }
                $model->file = $path;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Meeting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->start_at = date('d-M-Y h:i', $model->start_at);
        $model->end_at = date('d-M-Y h:i', $model->end_at);
        $model->start_actual_at = time();
        $model->end_actual_at = time();
        $model->status = Yii::t('app', 'Cancelled');
        $model->save();
        return $this->redirect(['meeting/project/' . $model->id_project]);
    }

    /**
     * Finds the Meeting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meeting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Meeting::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionStart($id, $flag = true) {
        $model = $this->findModel($id);
        $model->start_at = date('d-M-Y h:i', $model->start_at);
        $model->end_at = date('d-M-Y h:i', $model->end_at);
        if ($flag) {
            $model->start_actual_at = time();
            $model->status = Yii::t('app', 'Started');
        } else {
            $model->end_actual_at = time();
            $model->status = Yii::t('app', 'Finished');
        }
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionFile($id) {
        $model = $this->findModel($id);
        return Yii::$app->response->sendFile(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $model->file, 'notulen_' . $id . '_' . time() . '.pdf', ['inline' => true]);
    }

}
