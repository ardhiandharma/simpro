<?php

namespace frontend\controllers;

use Yii;
use common\models\Wiki;
use frontend\models\WikiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WikiController implements the CRUD actions for Wiki model.
 */
class WikiController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Wiki models.
     * @return mixed
     */
    public function actionIndex($id_project = null) {
        $searchModel = new WikiSearch();
        if (!is_null($id_project)) {
            $searchModel->id_project = $id_project;
        } else {
            $searchModel->status = 1;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(is_null($id_project) ? 'index_null' : 'index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_project' => $id_project,
        ]);
    }

    /**
     * Displays a single Wiki model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Wiki model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_project) {
        $model = new Wiki();
        $model->id_project = $id_project;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Wiki model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Wiki model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Wiki model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Wiki the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Wiki::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionApprove($id) {
        $model = $this->findModel($id);
        $model->status = 1;
        $model->updated_at = time();
        if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public function actionTimeline($id) {
        return $this->render('timeline', [
                    'model' => $this->findModel($id),
        ]);
    }

}
