<?php

namespace frontend\controllers;

use Yii;
use common\models\MeetingUser;
use frontend\models\MeetingUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Meeting;
use common\components\Notification;

/**
 * MeetingUserController implements the CRUD actions for MeetingUser model.
 */
class MeetingUserController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MeetingUser models.
     * @return mixed
     */
    public function actionIndex($id_meeting) {
        $searchModel = new MeetingUserSearch();
        $searchModel->id_meeting = $id_meeting;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_meeting' => $id_meeting,
                    'meeting' => Meeting::findOne(['id' => $id_meeting])
        ]);
    }

    /**
     * Creates a new MeetingUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_meeting) {
        $model = new MeetingUser();
        $model->id_meeting = $id_meeting;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!is_null($model->id_user)) {
                Notification::notify(Notification::KEY_MEETING_REMINDER, $model->id_user, $model->meeting->id);
            }
            return $this->redirect(['index', 'id_meeting' => $id_meeting]);
        }

        return $this->render('create', [
                    'model' => $model,
                    'id_meeting' => $id_meeting,
        ]);
    }

    /**
     * Deletes an existing MeetingUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['index', 'id_meeting' => $model->id_meeting]);
    }

    /**
     * Finds the MeetingUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeetingUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MeetingUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
