<?php

namespace frontend\controllers;

use Yii;
use common\models\Task;
use frontend\models\TaskOwnSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2fullcalendar\models\Event;
use yii\helpers\Url;

/**
 * TaskOwnController implements the CRUD actions for Task model.
 */
class TaskOwnController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TaskOwnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCalendar() {
        $searchModel = new TaskOwnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $events = [];
        foreach ($dataProvider->models as $model) {
            $e = new Event();
            $e->id = $model->id;
            $e->title = $model->name;
//            $e->description = $model->description;
            $e->start = date('Y-m-d\TH:i:s\Z', $model->start_plan_at);
            $e->end = date('Y-m-d\TH:i:s\Z', $model->end_plan_at);
            $e->className = 'event' . $model->id;
            $e->url = Url::to(['view', 'id' => $model->id]);
            $e->color = is_null($model->start_actual_at) ? 'blue' : 'green';
            $events[] = $e;
        }

        return $this->render('calendar', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'events' => $events
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionStart($id, $flag = true) {
        $model = $this->findModel($id);
        $model->start_plan_at = date('d-M-Y', $model->start_plan_at);
        $model->end_plan_at = date('d-M-Y', $model->end_plan_at);
        if ($flag) {
            $model->start_actual_at = time();
            $model->status = Yii::t('app', 'Started');
        } else {
            $model->end_actual_at = time();
            $model->status = Yii::t('app', 'Finished');
        }
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }

}
