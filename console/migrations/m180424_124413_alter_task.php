<?php

use yii\db\Migration;

/**
 * Class m180424_124413_alter_task
 */
class m180424_124413_alter_task extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('task', 'id_user', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('task', 'id_user');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180424_124413_alter_task cannot be reverted.\n";

      return false;
      }
     */
}
