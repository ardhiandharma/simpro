<?php

use yii\db\Migration;

/**
 * Class m180424_044153_create_project_users
 */
class m180424_044153_create_project_user extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('project_user', [
            'id' => $this->primaryKey(),
            'id_project' => $this->integer(),
            'id_user' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('project_user');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180424_044153_create_project_users cannot be reverted.\n";

      return false;
      }
     */
}
