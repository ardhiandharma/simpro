<?php

use yii\db\Migration;

/**
 * Class m180429_065251_create_wiki_user
 */
class m180429_065251_create_wiki_user extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('wiki_user', [
            'id' => $this->primaryKey(),
            'id_wiki' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('wiki_user');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180429_065251_create_wiki_user cannot be reverted.\n";

      return false;
      }
     */
}
