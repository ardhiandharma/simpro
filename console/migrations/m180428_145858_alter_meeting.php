<?php

use yii\db\Migration;

/**
 * Class m180428_145858_alter_meeting
 */
class m180428_145858_alter_meeting extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('meeting', 'file', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('meeting', 'file');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180428_145858_alter_meeting cannot be reverted.\n";

      return false;
      }
     */
}
