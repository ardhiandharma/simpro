<?php

use yii\db\Migration;

/**
 * Class m180428_141208_alter_meeting
 */
class m180428_141208_alter_meeting extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('meeting', 'start_actual_at', $this->integer());
        $this->addColumn('meeting', 'end_actual_at', $this->integer());
        $this->addColumn('meeting', 'status', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('meeting', 'start_actual_at');
        $this->dropColumn('meeting', 'end_actual_at');
        $this->dropColumn('meeting', 'status');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180428_141208_alter_meeting cannot be reverted.\n";

      return false;
      }
     */
}
