<?php

use yii\db\Migration;

/**
 * Class m180426_131527_create_meeting_user
 */
class m180426_131527_create_meeting_user extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('meeting_user', [
            'id' => $this->primaryKey(),
            'id_meeting' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('meeting_user');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180426_131527_create_meeting_user cannot be reverted.\n";

      return false;
      }
     */
}
