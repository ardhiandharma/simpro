<?php

use yii\db\Migration;

/**
 * Class m180424_043847_create_task
 */
class m180424_043847_create_task extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'id_project' => $this->integer()->notNull(),
            'id_sub_task' => $this->integer(),
            'name' => $this->string(100)->notNull(),
            'description' => $this->text(),
            'start_plan_at' => $this->integer()->notNull(),
            'end_plan_at' => $this->integer()->notNull(),
            'start_actual_at' => $this->integer(),
            'end_actual_at' => $this->integer(),
            'status' => $this->string(10)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('task');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180424_043847_create_task cannot be reverted.\n";

      return false;
      }
     */
}
