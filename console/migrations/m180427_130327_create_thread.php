<?php

use yii\db\Migration;

/**
 * Class m180427_130327_create_thread
 */
class m180427_130327_create_thread extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('discussion', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_project' => $this->integer()->notNull(),
            'subject' => $this->string()->notNull(),
            'message' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('discussion');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180427_130327_create_thread cannot be reverted.\n";

      return false;
      }
     */
}
