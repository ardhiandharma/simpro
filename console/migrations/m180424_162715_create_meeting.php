<?php

use yii\db\Migration;

/**
 * Class m180424_162715_create_meeting
 */
class m180424_162715_create_meeting extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('meeting', [
            'id' => $this->primaryKey(),
            'id_project' => $this->integer()->notNull(),
            'id_notulen' => $this->integer(),
            'name' => $this->string(100)->notNull(),
            'agenda' => $this->text()->notNull(),
            'start_at' => $this->integer()->notNull(),
            'end_at' => $this->integer()->notNull(),
            'location' => $this->string()->notNull(),
            'summary' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('meeting');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180424_162715_create_meeting cannot be reverted.\n";

      return false;
      }
     */
}
