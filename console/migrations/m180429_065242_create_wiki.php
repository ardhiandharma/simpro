<?php

use yii\db\Migration;

/**
 * Class m180429_065242_create_wiki
 */
class m180429_065242_create_wiki extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('wiki', [
            'id' => $this->primaryKey(),
            'id_project' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'content' => $this->text(),
            'status' => $this->integer(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('wiki');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180429_065242_create_wiki cannot be reverted.\n";

      return false;
      }
     */
}
