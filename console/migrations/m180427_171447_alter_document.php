<?php

use yii\db\Migration;

/**
 * Class m180427_171447_create_document
 */
class m180427_171447_alter_document extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('document', 'id_project', $this->integer()->notNull());
        $this->dropColumn('document', 'file');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('document', 'id_project');
        $this->addColumn('document', 'file', $this->binary()->notNull());
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180427_171447_create_document cannot be reverted.\n";

      return false;
      }
     */
}
