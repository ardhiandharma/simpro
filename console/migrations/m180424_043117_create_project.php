<?php

use yii\db\Migration;

/**
 * Class m180424_043117_create_project
 */
class m180424_043117_create_project extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'description' => $this->text(),
            'start_plan_at' => $this->integer()->notNull(),
            'end_plan_at' => $this->integer()->notNull(),
            'start_actual_at' => $this->integer(),
            'end_actual_at' => $this->integer(),
            'status' => $this->string(10)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('project');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180424_043117_create_project cannot be reverted.\n";

      return false;
      }
     */
}
