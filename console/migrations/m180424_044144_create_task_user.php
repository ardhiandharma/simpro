<?php

use yii\db\Migration;

/**
 * Class m180424_044144_create_task_users
 */
class m180424_044144_create_task_user extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('task_user', [
            'id' => $this->primaryKey(),
            'id_task' => $this->integer(),
            'id_user' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('task_user');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180424_044144_create_task_users cannot be reverted.\n";

      return false;
      }
     */
}
