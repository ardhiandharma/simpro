<?php

use yii\db\Migration;

/**
 * Class m180427_042435_create_document
 */
class m180427_042435_create_document extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('document', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'size' => $this->double()->notNull(),
            'type' => $this->string()->notNull(),
            'file' => $this->binary()->notNull(),
            'filename' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('document');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180427_042435_create_document cannot be reverted.\n";

      return false;
      }
     */
}
